# frozen_string_literal: true

require './lib/micro_api'

run Rack::Cascade.new([App])
