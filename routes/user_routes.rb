# frozen_string_literal: true

# Routes e Controller for User
App.class_eval do
  hash_branch('user') do |r|
    _request = Support::ApiResponse.new r

    # POST user/create
    r.post 'create' do
      Controllers::User.create(_request)
    end

    r.on 'find' do
      user = ContextUser::Sessions.user(_request.token)
      _request.unauthorized_user if user.nil?

      # GET user/find/show
      r.get 'show' do
        _request.success user
      end

      # PUT user/find/update
      r.put 'update' do
        params = _request.params

        Worker::ContextUser.perform_async(params.merge({id: user.id}), :update)

        _request.success "request sent to queue"
      end

      # DELETE user/find/delete
      r.delete 'delete' do
        Worker::ContextUser.perform_async({id: user.id}, :destroy)

        _request.success "request sent to queue"
      end

      # POST user/find/denunciation
      r.post 'denunciation' do
        Controllers::Denunciation.create(user, _request)
      end
    end
  end
end