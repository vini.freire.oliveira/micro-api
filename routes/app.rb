# frozen_string_literal: true

# Routes e Controller for App
class App < Roda
  include Support::RodaPlugins

  require_relative 'user_routes'
  require_relative 'operator_routes'

  route do |r|
    _request = Support::ApiResponse.new r

    r.root do
      msg = { status: 'online', version: VERSION.to_s }

      _request.success msg
    end

    r.on 'auth' do
      r.post 'login' do
        params = _request.params

        token = ContextUser::Sessions.create(params)

        return _request.success({token: token}) if token

        _request.unauthorized_user
      end

      r.get 'logout' do        
        ContextUser::Sessions.destroy(r.headers['token'])

        _request.accepted('logout')
      end
    end

    r.hash_branches
  end
end
