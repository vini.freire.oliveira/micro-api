# frozen_string_literal: true

# Routes e Controller for User
App.class_eval do
  hash_branch('operator') do |r|
    _request = Support::ApiResponse.new r

    user = ContextUser::Sessions.user(_request.token, true)
    _request.unauthorized_user if user.nil?

    r.on 'denunciations' do
      # GET operator/denunciations/all
      r.get 'all' do
        Controllers::Denunciation.all(_request)
      end

      # PUT operator/denunciations/attend
      r.put 'attend' do
        denunciation = Denunciation.find _request.params[:denunciation_id]
        _request.not_found if denunciation.nil?

        Controllers::Denunciation.attend(_request, denunciation.serializable_hash)
      end
    end

    r.on 'histories' do
      # GET operator/histories/all
      r.get 'all' do
        Controllers::History.all(_request)
      end
    end
  end
end