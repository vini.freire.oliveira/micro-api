# frozen_string_literal: true

module Worker
  # publish message rabbitmq with denunciation operation
  class ContextDenunciation
    include Sidekiq::Worker
    sidekiq_options queue: :denunciations

    def perform(params, action)
      Services::Publisher.call(params, :denunciation, action)
    end
  end
end
