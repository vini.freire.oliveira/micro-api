# frozen_string_literal: true

module Worker
  # publish message rabbitmq with denunciation operation
  class ContextHistory
    include Sidekiq::Worker
    sidekiq_options queue: :histories

    def perform(params, action)
      Services::Publisher.call(params, :history, action)
    end
  end
end
