# frozen_string_literal: true

module Worker
  # publish message rabbitmq with create_user operation
  class ContextUser
    include Sidekiq::Worker
    sidekiq_options queue: :users

    def perform(params, action)
      Services::Publisher.call(params, :user, action)
    end
  end
end
