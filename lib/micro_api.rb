# frozen_string_literal: true

require 'erb'
require 'forwardable'
require 'json'
require 'roda'
require 'bunny'
require 'redis'
require 'sidekiq'

# databases
require 'context_user_database'
require 'context_denunciation_database'
require 'context_history_database'

# Load support files
Dir["#{Dir.pwd}/lib/support/**/*.rb"].sort.each { |file| require file }

# Load Web Stories routes
require './routes/app'

# Load Workers
require_relative 'workers/checker'
require_relative 'workers/context_user'
require_relative 'workers/context_denunciation'
require_relative 'workers/context_history'

# Load api Service
Dir["#{Dir.pwd}/lib/services/**/*.rb"].sort.each { |file| require file }

# Load api Controllers
Dir["#{Dir.pwd}/controllers/**/*.rb"].sort.each { |file| require file }

APP_NAME = ENV.fetch('APP_NAME', 'micro-api')
RUBY_ENV = ENV.fetch('RUBY_ENV', 'development').downcase
VERSION = File.read('version.txt').chomp

def set_client_sidekiq
  Sidekiq.configure_client do |config|
    config.redis = { url: ENV['REDIS_HOST'] }
  end
end

set_client_sidekiq if ENV['APP_NAME'] == 'micro-api'
