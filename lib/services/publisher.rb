# frozen_string_literal: true

# Publisher to RabbitMQ queue
require "#{ENV['SHARED_RESOURCES']}/services/publisher.rb"
