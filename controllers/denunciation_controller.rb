# frozen_string_literal: true

module Controllers
  # Denunciation Controllers
  class Denunciation
    class << self
      def create(user, _request)
        params = _request.params.merge({user_id: user.id})

        begin
          @protocol = ContextDenunciation::Validation.run params
        rescue ContextDenunciationException => error
          puts "ContextDenunciation: #{error.message}"
  
          _request.failure error.message
        else
          Worker::ContextDenunciation.perform_async(params.merge({protocol: @protocol}), :create)
  
          _request.created "#{@protocol}"
        end
      end

      def attend(_request, params)
        Worker::ContextHistory.perform_async(params, :create)
        Worker::ContextDenunciation.perform_async({protocol: params["protocol"]}, :destroy)

        _request.created "denunciation: #{params["protocol"]} attend"
      end

      def all(_request)
        denunciations = ::Denunciation.all

        _request.success denunciations
      end
    end
  end
end
