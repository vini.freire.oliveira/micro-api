# frozen_string_literal: true

module Controllers
  # Denunciation Controllers
  class User
    class << self
      def create(_request)
        params = _request.params

        begin
          @uid = ContextUser::Validation.run params
        rescue ContextUserException => error
          puts "ContextUser: #{error.message}"
  
          _request.failure error.message
        else
          Worker::ContextUser.perform_async(params.merge({uid: @uid}), :create)
  
          _request.created "request sent to queue: #{@uid}"
        end
      end
    end
  end
end