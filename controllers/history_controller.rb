# frozen_string_literal: true

module Controllers
  # History Controllers
  class History
    class << self
      def all(_request)
        histories = ::History.all

        _request.success histories
      end
    end
  end
end
